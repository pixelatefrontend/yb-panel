
$(document).ready(function(){

	"use strict";

    //*** Twitter Feed Widget ***//
    $('#tweecool').tweecool({
            //settings
            username : 'envato',
            limit : 4
        });


        //*** Piety Mini Charts ***//
        $(function() {
        	$(".bar").peity("bar", {
        		fill: ["#ff8484"],
        		height: ["40px"],
        		width: ["94px"]
        	})

        	$(".bar2").peity("bar", {
        		fill: ["#9797ff"],
        		height: ["40px"],
        		width: ["94px"]
        	})

        	$(".data-attributes span").peity("donut")
        });

        
        //***** Clients lists Scroll *****//
        $(function(){
        	$('#people-list').slimScroll({
        		height: '233px',
        		wheelStep: 10,
        		size: '2px'
        	});
        });
        

        //***** Twitter Widget Scroll *****//
        $(function(){
        	$('.twitter-widget').slimScroll({
        		height: '282px',
        		wheelStep: 10,
        		size: '2px'
        	});
        });

        //*** Activity Chart ***//
        function updateChartData(object, data) {
        	for (var i = 0; i < data.length; i++) {
        		object[i].value = data[i];
        	}
        };

        var data = {
        	labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "Octuber", "Noverber", "December"],
        	datasets: [
        	{
        		label: "My First dataset",
        		fillColor: "rgba(255,162,162,0.2)",
        		strokeColor: "rgba(255,162,162,1)",
        		pointColor: "rgba(255,162,162,1)",
        		pointStrokeColor: "#fff",
        		pointHighlightFill: "#fff",
        		pointHighlightStroke: "rgba(220,220,220,1)",
        		data: [65, 59, 120, 200, 130, 100, 180, 150, 120, 170, 210, 350]
        	},
        	{
        		label: "My Second dataset",
        		fillColor: "rgba(173,173,253,0.2)",
        		strokeColor: "rgba(173,173,253,1)",
        		pointColor: "rgba(173,173,253,1)",
        		pointStrokeColor: "#fff",
        		pointHighlightFill: "#fff",
        		pointHighlightStroke: "rgba(173,173,253,1)",
        		data: [100, 200, 150, 300, 90, 180, 350, 100, 400, 200, 50, 400]
        	}
        	]
        };

        var ctx = document.getElementById("chart").getContext("2d");
        var myLineChart = new Chart(ctx).Line(data, {
        	multiTooltipTemplate: "<%= value %>%",
        	legendTemplate: '',
        	scaleShowLabels: false,
        	scaleShowGridLines : true,
        	pointDotRadius : 6,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 2,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,
            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(0,0,0,.0)",


            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
        });


            //Sort by first name
            $(function() {
            	$.fn.sortList = function() {
            		var mylist = $(this);
            		var listitems = $('li', mylist).get();
            		listitems.sort(function(a, b) {
            			var compA = $(a).text().toUpperCase();
            			var compB = $(b).text().toUpperCase();
            			return (compA < compB) ? -1 : 1;
            		});
            		$.each(listitems, function(i, itm) {
            			mylist.append(itm);
            		});
            	}
            });

      //Sort by last name
      $(function() {
      	$.fn.sortListLast = function() {
      		var mylist = $(this);
      		var listitems = $('li', mylist).get();
      		listitems.sort(function(a, b) {
      			var compA = $('.last-name', a).text().toUpperCase();
      			var compB = $('.last-name', b).text().toUpperCase();
      			return (compA < compB) ? -1 : 1;
      		});
      		$.each(listitems, function(i, itm) {
      			mylist.append(itm);
      		});
      	}
      });

    //Search filter
    (function ($) {
        // custom css expression for a case-insensitive contains()
        jQuery.expr[':'].Contains = function(a,i,m){
        	return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
        };


        function listFilter(searchDir, list) { 
        	var form = $("<form>").attr({"class":"filterform","action":"#"}),
        	input = $("<input>").attr({"class":"filterinput","type":"text","placeholder":"Live Client Search"});
        	$(form).append(input).appendTo(searchDir);

        	$(input)
        	.change( function () {
        		var filter = $(this).val();
        		if(filter) {
        			$(list).find("li:not(:Contains(" + filter + "))").slideUp();
        			$(list).find("li:Contains(" + filter + ")").slideDown();
        		} else {
        			$(list).find("li").slideDown();
        		}
        		return false;
        	})
        	.keyup( function () {
        		$(this).change();
        	});
        }

    //ondomready
    $(function () {
    	listFilter($("#searchDir"), $("#people-list"));
    });
}(jQuery));


    //*** Task lists ***//
    $('.task-managment > ol > li').on("click", function(){
    	$(this).toggleClass('active');
    });

    //*** Task List Deleted ***//
    $('.task-managment > ol > li > span').on("click", function(){
    	$(this).parent().slideUp();
    });

    var counter = 0;
    $('button#addEventAdder, button#addEventAdder > i').on('click',function(){
    	var classes = ["red-skin", "sky-skin", "purple-skin", "pink-skin", "orange-skin"];
    	var ul = $('ol#listitems');
    	var item = $('input#task-item').val();
    	if(item !== '' && item !== 'undefined' ){
    		if(counter == classes.length){
    			counter = 0;
    		}
    		$(ul).prepend('<li><i class="empty"></i>'+item+'<span class="fa fa-close"></span></li>');
    		$('.task-managment > ol > li').on("click", function(){
    			$(this).toggleClass('active');
    		});
    		$('.task-managment > ol > li > span').on("click", function(){
    			$(this).parent().slideUp();
    		});
    		$('input#task-item').addClass('null');
    		$('input#task-item').val('');
    		$('input#task-item').focus();
    		var attribute = $("ol#listitems").children('li').eq(0).children('i');
    		$(attribute).attr('class', classes[counter]);
    		counter++;
    	}
    	
    });

    //*** Chat message Initiliaze ***//
    var KEY_ENTER=13;
    var $input=$(".chat-input")
    ,$sendButton=$(".chat-send")
    ,$messagesContainer=$(".chat-messages")
    ,$messagesList=$(".chat-messages-list")
    ,$effectContainer=$(".chat-effect-container")
    ,$infoContainer=$(".chat-info-container")

    ,messages=0
    ,bleeding=100
    ,isFriendTyping=false
    ,incomingMessages=0
    ,lastMessage=""
    ;
    
    var lipsum="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

    function gooOn(){
    	setFilter('url(#goo)');
    }
    function gooOff(){
    	setFilter('none');
    }
    function setFilter(value){
    	$effectContainer.css({
    		webkitFilter:value,
    		mozFilter:value,
    		filter:value,
    	});
    }

    function addMessage(message,self){
    	var $messageContainer=$("<li/>")
    	.addClass('chat-message '+(self?'chat-message-self':'chat-message-friend'))
    	.appendTo($messagesList)
    	;
    	var $messageBubble=$("<div/>")
    	.addClass('chat-message-bubble')
    	.appendTo($messageContainer)
    	;
    	$messageBubble.text(message);

    	var oldScroll=$messagesContainer.scrollTop();
    	$messagesContainer.scrollTop(9999999);
    	var newScroll=$messagesContainer.scrollTop();
    	var scrollDiff=newScroll-oldScroll;
    	TweenMax.fromTo(
    		$messagesList,0.4,{
    			y:scrollDiff
    		},{
    			y:0,
    			ease:Quint.easeOut
    		}
    		);

    	return {
    		$container:$messageContainer,
    		$bubble:$messageBubble
    	};
    }
    function sendMessage(){
    	var message=$input.text();
    	
    	if(message=="") return;
    	
    	lastMessage=message;

    	var messageElements=addMessage(message,true)
    	,$messageContainer=messageElements.$container
    	,$messageBubble=messageElements.$bubble
    	;

    	var oldInputHeight=$(".chat-input-bar").height();
    	$input.text('');
    	updateChatHeight();
    	var newInputHeight=$(".chat-input-bar").height();
    	var inputHeightDiff=newInputHeight-oldInputHeight

    	var $messageEffect=$("<div/>")
    	.addClass('chat-message-effect')
    	.append($messageBubble.clone())
    	.appendTo($effectContainer)
    	.css({
    		left:$input.position().left-12,
    		top:$input.position().top+bleeding+inputHeightDiff
    	})
    	;


    	var messagePos=$messageBubble.offset();
    	var effectPos=$messageEffect.offset();
    	var pos={
    		x:messagePos.left-effectPos.left,
    		y:messagePos.top-effectPos.top
    	}

    	var $sendIcon=$sendButton.children("i");
    	TweenMax.to(
    		$sendIcon,0.15,{
    			x:30,
    			y:-30,
    			force3D:true,
    			ease:Quad.easeOut,
    			onComplete:function(){
    				TweenMax.fromTo(
    					$sendIcon,0.15,{
    						x:-30,
    						y:30
    					},
    					{
    						x:0,
    						y:0,
    						force3D:true,
    						ease:Quad.easeOut
    					}
    					);
    			}
    		}
    		);

    	gooOn();

    	
    	TweenMax.from(
    		$messageBubble,0.8,{
    			y:-pos.y,
    			ease:Sine.easeInOut,
    			force3D:true
    		}
    		);

    	var startingScroll=$messagesContainer.scrollTop();
    	var curScrollDiff=0;
    	var effectYTransition;
    	var setEffectYTransition=function(dest,dur,ease){
    		return TweenMax.to(
    			$messageEffect,dur,{
    				y:dest,
    				ease:ease,
    				force3D:true,
    				onUpdate:function(){
    					var curScroll=$messagesContainer.scrollTop();
    					var scrollDiff=curScroll-startingScroll;
    					if(scrollDiff>0){
    						curScrollDiff+=scrollDiff;
    						startingScroll=curScroll;

    						var time=effectYTransition.time();
    						effectYTransition.kill();
    						effectYTransition=setEffectYTransition(pos.y-curScrollDiff,0.8-time,Sine.easeOut);
    					}
    				}
    			}
    			);
    	}

    	effectYTransition=setEffectYTransition(pos.y,0.8,Sine.easeInOut);
    	
            // effectYTransition.updateTo({y:800});

            TweenMax.from(
            	$messageBubble,0.6,{
            		delay:0.2,
            		x:-pos.x,
            		ease:Quad.easeInOut,
            		force3D:true
            	}
            	);
            TweenMax.to(
            	$messageEffect,0.6,{
            		delay:0.2,
            		x:pos.x,
            		ease:Quad.easeInOut,
            		force3D:true
            	}
            	);

            TweenMax.from(
            	$messageBubble,0.2,{
            		delay:0.65,
            		opacity:0,
            		ease:Quad.easeInOut,
            		onComplete:function(){
            			TweenMax.killTweensOf($messageEffect);
            			$messageEffect.remove();
            			if(!isFriendTyping)
            				gooOff();
            		}
            	}
            	);

            messages++;

            if(Math.random()<0.65 || lastMessage.indexOf("?")>-1 || messages==1) getReply();
        }
        function getReply(){
        	if(incomingMessages>2) return;
        	incomingMessages++;
        	var typeStartDelay=1000+(lastMessage.length*40)+(Math.random()*1000);
        	setTimeout(friendIsTyping,typeStartDelay);

        	var source=lipsum.toLowerCase();
        	source=source.split(" ");
        	var start=Math.round(Math.random()*(source.length-1));
        	var length=Math.round(Math.random()*13)+1;
        	var end=start+length;
        	if(end>=source.length){
        		end=source.length-1;
        		length=end-start;
        	}
        	var message="";
        	for (var i = 0; i < length; i++) {
        		message+=source[start+i]+(i<length-1?" ":"");
        	};
        	message+=Math.random()<0.4?"?":"";
        	message+=Math.random()<0.2?" :)":(Math.random()<0.2?" :(":"");

        	var typeDelay=300+(message.length*50)+(Math.random()*1000);

        	setTimeout(function(){
        		receiveMessage(message);
        	},typeDelay+typeStartDelay);

        	setTimeout(function(){
        		incomingMessages--;
        		if(Math.random()<0.1){
        			getReply();
        		}
        		if(incomingMessages<=0){
        			friendStoppedTyping();
        		}
        	},typeDelay+typeStartDelay);
        }
        function friendIsTyping(){
        	if(isFriendTyping) return;

        	isFriendTyping=true;

        	var $dots=$("<div/>")
        	.addClass('chat-effect-dots')
        	.css({
        		top:-30+bleeding,
        		left:10
        	})
        	.appendTo($effectContainer)
        	;
        	for (var i = 0; i < 3; i++) {
        		var $dot=$("<div/>")
        		.addClass("chat-effect-dot")
        		.css({
        			left:i*20
        		})
        		.appendTo($dots)
        		;
        		TweenMax.to($dot,0.3,{
        			delay:-i*0.1,
        			y:30,
        			yoyo:true,
        			repeat:-1,
        			ease:Quad.easeInOut
        		})
        	};

        	var $info=$("<div/>")
        	.addClass("chat-info-typing")
        	.text("Your friend is typing...")
        	.css({
        		transform:"translate3d(0,30px,0)"
        	})
        	.appendTo($infoContainer)

        	TweenMax.to($info, 0.3,{
        		y:0,
        		force3D:true
        	});

        	gooOn();
        }

        function friendStoppedTyping(){
        	if(!isFriendTyping) return

        		isFriendTyping=false;

        	var $dots=$effectContainer.find(".chat-effect-dots");
        	TweenMax.to($dots,0.3,{
        		y:40,
        		force3D:true,
        		ease:Quad.easeIn,
        	});

        	var $info=$infoContainer.find(".chat-info-typing");
        	TweenMax.to($info,0.3,{
        		y:30,
        		force3D:true,
        		ease:Quad.easeIn,
        		onComplete:function(){
        			$dots.remove();
        			$info.remove();

        			gooOff();
        		}
        	});
        }
        function receiveMessage(message){
        	var messageElements=addMessage(message,false)
        	,$messageContainer=messageElements.$container
        	,$messageBubble=messageElements.$bubble
        	;

        	TweenMax.set($messageBubble,{
        		transformOrigin:"60px 50%"
        	})
        	TweenMax.from($messageBubble,0.4,{
        		scale:0,
        		force3D:true,
        		ease:Back.easeOut
        	})
        	TweenMax.from($messageBubble,0.4,{
        		x:-100,
        		force3D:true,
        		ease:Quint.easeOut
        	})
        }

        function updateChatHeight(){
        	$messagesContainer.css({
        		height:460-$(".chat-input-bar").height()
        	});
        }

        $input.keydown(function(event) {
        	if(event.keyCode==KEY_ENTER){
        		event.preventDefault();
        		sendMessage();
        	}
        });
        $sendButton.click(function(event){
        	event.preventDefault();
        	sendMessage();
            // $input.focus();
        });
        $sendButton.on("touchstart",function(event){
        	event.preventDefault();
        	sendMessage();
            // $input.focus();
        });

        $input.on("input",function(){
        	updateChatHeight();
        });

        gooOff();
        updateChatHeight();


    });
    $('button#btnCreate').click(function(){
        var location = $(this).data('href');
        window.location.href = location;
    });
    $('[data-toggle="tooltip"]').tooltip();

    function destroyItem(item){
        swal({
            title: 'Uyarı!',
            text: "Bu öğeyi silmek istediğinden emin misin?",
            type: 'warning',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sil',
            cancelButtonText: 'İptal',
            showCancelButton: true,
          }).then(function () {
            $(item).parent('form').submit();
            swal(
              'Silindi!',
              'Silme işlemi başarılı',
              'success',
            )
          });
    }