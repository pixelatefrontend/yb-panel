import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
      token: localStorage.getItem('access_token') || null,
    },
    mutations: {
      retrieveToken(state, token) {
        state.token = token
      },
      destroyToken(state) {
        state.token = null
      }
    },
    actions: {
      retrieveToken(context, credentials) {
        return new Promise((resolve, reject) => {
          axios.post('login', {
            email: credentials.email,
            password: credentials.password
          }).then(response => {
            const token = response.data.access_token
            Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
            localStorage.setItem('access_token', token)
            context.commit('retrieveToken', token)
            resolve(response)
          }).catch(error => {
            // console.log(error)
            reject(error)
          })
        })
      },
      destroyToken(context) {
        if(context.getters.loggedIn) {
          return new Promise((resolve, reject) => {
            axios.post('logout').then(response => {
              localStorage.removeItem('access_token')
              context.commit('destroyToken')
              resolve(response)
            }).catch(error => {
              localStorage.removeItem('access_token')
              context.commit('destroyToken')
              reject(error)
            })
          })
        }
      }
    },
    getters: {
      loggedIn(state) {
        return state.token !== null
      }
    }
})
