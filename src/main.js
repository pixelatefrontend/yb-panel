import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App.vue'
import router from './router'
import store from './store'
import VueProgressBar from 'vue-progressbar'
import {Tabs, Tab} from 'vue-tabs-component'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSweetalert2 from 'vue-sweetalert2'
import FileManager from 'laravel-file-manager'
import VueCloneya from 'vue-cloneya'
import VueEditor from 'vue2-editor'


Vue.use(VueCloneya);
window.Slug = require('slug')
Vue.use(require('vue-moment'))
Vue.use(FileManager, {
    store,
    headers: {'X-CSRF-TOKEN': 'token'},
    baseUrl: process.env.VUE_APP_API_URL + 'file-manager/',
    windowsConfig: 2,
})


Vue.config.productionTip = false


const options = {
    color: '#00e676',
    failedColor: '#874b4b',
    thickness: '5px',
    transition: {
        speed: '0.5s',
        opacity: '0.6s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
}
Vue.use(VueProgressBar, options)
Vue.use(VueSweetalert2);
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAQ9dVDUX649BQ4MVjTn9p9jF1Qmzy_RzI',
        libraries: 'places',
    },
    installComponents: true,
})
Vue.component('tabs', Tabs)
Vue.component('tab', Tab)
Vue.component('vue-editor', VueEditor)


const BASE_API_URL = process.env.VUE_APP_API_URL

Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = BASE_API_URL
Vue.axios.defaults.headers.common['Accept'] = 'application/json'
Vue.axios.defaults.headers.common['Content-Type'] = 'application/json'
const token = store.state.token
if (token) {
    Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
}

function checkToken() {
    const token = localStorage.getItem('access_token');
    if (token) {
        return true
    } else {
        return false
    }
}

router.beforeEach((to, from, next) => {
    if (to.path != '/login') {
        if (checkToken()) {
            next();
        } else {
            next('login');
        }
    } else {
        next();
    }
});

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
