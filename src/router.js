import Vue from 'vue'
import Router from 'vue-router'
// AuthLogin
import AuthLogin from './views/auth/Login.vue'
import AuthLogout from './views/auth/Logout.vue'
// Dashboard
import Dashboard from './views/Dashboard.vue'
// Yurtlar
import DormList from './views/dorms/List.vue'
import DormCreate from './views/dorms/Create.vue'
import DormUpdate from './views/dorms/Update.vue'
// Üyeler
import UserList from './views/users/List.vue'
import UserCreate from './views/users/Create.vue'
import UserUpdate from './views/users/Update.vue'
// Reklamlar
import PromoteList from './views/promotes/List.vue'
import PromoteCreate from './views/promotes/Create.vue'
import PromoteUpdate from './views/promotes/Update.vue'
// Haberler
import NewsList from './views/news/List.vue'
import NewsCreate from './views/news/Create.vue'
import NewsUpdate from './views/news/Update.vue'
// Üniversiteler
import UniversityList from './views/universities/List.vue'
import UniversityCreate from './views/universities/Create.vue'
import UniversityUpdate from './views/universities/Update.vue'
// Fakülteler
import FacultyList from './views/faculties/List.vue'
import FacultyCreate from './views/faculties/Create.vue'
import FacultyUpdate from './views/faculties/Update.vue'
// İlan Yönetimi
import AdvertList from './views/adverts/List.vue'
import AdvertCreate from './views/adverts/Create.vue'
import AdvertUpdate from './views/adverts/Update.vue'
// Sayfalar Yönetimi
import PageList from './views/pages/List.vue'
import PageCreate from './views/pages/Create.vue'
import PageUpdate from './views/pages/Update.vue'
// Blog Yönetimi
import BlogList from './views/blogs/List.vue'
import BlogCreate from './views/blogs/Create.vue'
import BlogUpdate from './views/blogs/Update.vue'
// Blog Kategori Yönetimi
import BlogCategoryList from './views/blog_categories/List.vue'
import BlogCategoryCreate from './views/blog_categories/Create.vue'
import BlogCategoryUpdate from './views/blog_categories/Update.vue'
// Banner Yönetimi
import BannerList from './views/banners/List.vue'
import BannerCreate from './views/banners/Create.vue'
import BannerUpdate from './views/banners/Update.vue'
// Popup Yönetimi
import PopupList from './views/popups/List.vue'
import PopupCreate from './views/popups/Create.vue'
import PopupUpdate from './views/popups/Update.vue'
// İletişim Formu
import ContactList from './views/contacts/List.vue'
import ContactShow from './views/contacts/Show.vue'
// Mesajlar
import MessageList from './views/messages/List.vue'
import MessageShow from './views/messages/Show.vue'
// Yurt Özellikleri
import DormPropertyList from './views/dorm_properties/List.vue'
import DormPropertyCreate from './views/dorm_properties/Create.vue'
import DormPropertyUpdate from './views/dorm_properties/Update.vue'
// Yurt Kategorileri
import DormCategoryList from './views/dorm_categories/List.vue'
import DormCategoryCreate from './views/dorm_categories/Create.vue'
import DormCategoryUpdate from './views/dorm_categories/Update.vue'
// İller
import CityList from './views/cities/List.vue'
import CityCreate from './views/cities/Create.vue'
import CityUpdate from './views/cities/Update.vue'
// İlçeler
import DistrictList from './views/districts/List.vue'
import DistrictCreate from './views/districts/Create.vue'
import DistrictUpdate from './views/districts/Update.vue'
// Site Ayarları
import SiteSetting from './views/site_settings/Index.vue'
// Panel Kullanıcıları
import PanelUserList from './views/panel_users/List.vue'
import PanelUserCreate from './views/panel_users/Create.vue'
import PanelUserUpdate from './views/panel_users/Update.vue'
// Dosya Kütüphanesi
import FileManager from './views/FileManager.vue'
// For Test
import Test from './views/Test.vue'



Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#ffb000' },
            { call: 'fail', modifier: 'temp', argument: '#6e0000' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '1.5s', opacity: '0.6s', termination: 400 } }
          ]
        }
      }
    },

    // Panel Authentication
    { path: '/login', name: 'panel.login', component: AuthLogin },
    { path: '/logout', name: 'panel.logout', component: AuthLogout },

    // Yurtlar Route (Liste, Ekle, Düzenle)
    { path: '/dorms', name: 'dorms.list', component: DormList, meta: { requiresAuth: true } },
    { path: '/dorms/create', name: 'dorms.create', component: DormCreate, meta: { requiresAuth: true } },
    { path: '/dorms/update/:id', name: 'dorms.update', component: DormUpdate, meta: { requiresAuth: true } },

    // Üyeler Route (Liste, Ekle, Düzenle)
    { path: '/users', name: 'users.list', component: UserList, meta: { requiresAuth: true } },
    { path: '/users/create', name: 'users.create', component: UserCreate, meta: { requiresAuth: true } },
    { path: '/users/update/:id', name: 'users.update', component: UserUpdate, meta: { requiresAuth: true } },


    // Üniversiteler Route (Liste, Ekle, Düzenle)
    { path: '/universities', name: 'universities.list', component: UniversityList, meta: { requiresAuth: true } },
    { path: '/universities/create', name: 'universities.create', component: UniversityCreate, meta: { requiresAuth: true } },
    { path: '/universities/update/:id', name: 'universities.update', component: UniversityUpdate, meta: { requiresAuth: true } },

    // Fakülteler Route (Liste, Ekle, Düzenle)
    { path: '/faculties', name: 'faculties.list', component: FacultyList, meta: { requiresAuth: true } },
    { path: '/faculties/create', name: 'faculties.create', component: FacultyCreate, meta: { requiresAuth: true } },
    { path: '/faculties/update/:id', name: 'faculties.update', component: FacultyUpdate, meta: { requiresAuth: true } },

    // ilan Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/adverts', name: 'adverts.list', component: AdvertList, meta: { requiresAuth: true } },
    { path: '/adverts/create', name: 'adverts.create', component: AdvertCreate, meta: { requiresAuth: true } },
    { path: '/adverts/update/:id', name: 'adverts.update', component: AdvertUpdate, meta: { requiresAuth: true } },

    // Reklam Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/promotes', name: 'promotes.list', component: PromoteList, meta: { requiresAuth: true } },
    { path: '/promotes/create', name: 'promotes.create', component: PromoteCreate, meta: { requiresAuth: true } },
    { path: '/promotes/update/:id', name: 'promotes.update', component: PromoteUpdate, meta: { requiresAuth: true } },

    // Haber Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/news', name: 'news.list', component: NewsList, meta: { requiresAuth: true } },
    { path: '/news/create', name: 'news.create', component: NewsCreate, meta: { requiresAuth: true } },
    { path: '/news/update/:id', name: 'news.update', component: NewsUpdate, meta: { requiresAuth: true } },

    // Sayfa Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/pages', name: 'pages.list', component: PageList, meta: { requiresAuth: true } },
    { path: '/pages/create', name: 'pages.create', component: PageCreate, meta: { requiresAuth: true } },
    { path: '/pages/update/:id', name: 'pages.update', component: PageUpdate, meta: { requiresAuth: true } },

    // Yurt Özellikleri Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/dorm_properties', name: 'dorm_properties.list', component: DormPropertyList, meta: { requiresAuth: true } },
    { path: '/dorm_properties/create', name: 'dorm_properties.create', component: DormPropertyCreate, meta: { requiresAuth: true } },
    { path: '/dorm_properties/update/:id', name: 'dorm_properties.update', component: DormPropertyUpdate, meta: { requiresAuth: true } },

    // Yurt Kategorileri Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/dorm_categories', name: 'dorm_categories.list', component: DormCategoryList, meta: { requiresAuth: true } },
    { path: '/dorm_categories/create', name: 'dorm_categories.create', component: DormCategoryCreate, meta: { requiresAuth: true } },
    { path: '/dorm_categories/update/:id', name: 'dorm_categories.update', component: DormCategoryUpdate, meta: { requiresAuth: true } },

    // İller Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/cities', name: 'cities.list', component: CityList, meta: { requiresAuth: true } },
    { path: '/cities/create', name: 'cities.create', component: CityCreate, meta: { requiresAuth: true } },
    { path: '/cities/update/:id', name: 'cities.update', component: CityUpdate, meta: { requiresAuth: true } },

    // İlçeler Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/districts', name: 'districts.list', component: DistrictList, meta: { requiresAuth: true } },
    { path: '/districts/create', name: 'districts.create', component: DistrictCreate, meta: { requiresAuth: true } },
    { path: '/districts/update/:id', name: 'districts.update', component: DistrictUpdate, meta: { requiresAuth: true } },

    // Popup Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/popups', name: 'popups.list', component: PopupList, meta: { requiresAuth: true } },
    { path: '/popups/create', name: 'popups.create', component: PopupCreate, meta: { requiresAuth: true } },
    { path: '/popups/update/:id', name: 'popups.update', component: PopupUpdate, meta: { requiresAuth: true } },

    // Blog Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/blogs', name: 'blogs.list', component: BlogList, meta: { requiresAuth: true } },
    { path: '/blogs/create', name: 'blogs.create', component: BlogCreate, meta: { requiresAuth: true } },
    { path: '/blogs/update/:id', name: 'blogs.update', component: BlogUpdate, meta: { requiresAuth: true } },

    // Blog Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/blog_categories', name: 'blog_categories.list', component: BlogCategoryList, meta: { requiresAuth: true } },
    { path: '/blog_categories/create', name: 'blog_categories.create', component: BlogCategoryCreate, meta: { requiresAuth: true } },
    { path: '/blog_categories/update/:id', name: 'blog_categories.update', component: BlogCategoryUpdate, meta: { requiresAuth: true } },

    // Banner Yönetimi Route (Liste, Ekle, Düzenle)
    { path: '/banners', name: 'banners.list', component: BannerList, meta: { requiresAuth: true } },
    { path: '/banners/create', name: 'banners.create', component: BannerCreate, meta: { requiresAuth: true } },
    { path: '/banners/update/:id', name: 'banners.update', component: BannerUpdate, meta: { requiresAuth: true } },

    // İletişim Formu Route (Liste)
    { path: '/contacts', name: 'contacts.list', component: ContactList, meta: { requiresAuth: true } },
    { path: '/contacts/show/:id', name: 'contacts.show', component: ContactShow, meta: { requiresAuth: true } },

    // Mesajlar Route (Liste)
    { path: '/messages', name: 'messages.list', component: MessageList, meta: { requiresAuth: true } },
    { path: '/messages/show/:id', name: 'messages.show', component: MessageShow, meta: { requiresAuth: true } },

    // Site Ayarları
    { path: '/settings', name: 'settings.index', component: SiteSetting, meta: { requiresAuth: true } },

    // Panel Kullanıcıları Route (Liste, Ekle, Düzenle)
    { path: '/panel_users', name: 'panel_users.list', component: PanelUserList, meta: { requiresAuth: true } },
    { path: '/panel_users/create', name: 'panel_users.create', component: PanelUserCreate, meta: { requiresAuth: true } },
    { path: '/panel_users/update/:id', name: 'panel_users.update', component: PanelUserUpdate, meta: { requiresAuth: true } },


    // Dosya Kütüphanesi
    { path: '/filemanager', name: 'filemanager', component: FileManager, meta: { requiresAuth: true } },

    /*- ----- -- -- - -- */
    // For Test
    { path: '/test', name: 'test', component: Test }
  ]

})
